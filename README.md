<img align="right" src="https://i.imgur.com/Qg4Hxry.png" width="200" height="200"/>

# ghost
*"I am a Ghost. More importantly, I am your Ghost. And you are one of the Traveler's chosen."*

Ghost is a private, work-in-progress [Discord](https://discordapp.com/) bot built with [JDA](https://github.com/DV8FromTheWorld/JDA).

## Planned features
* JavaFX frontend
  * Always-online system
  * Remote management
  * GUI-based configuration
  * Command line fallback
  * Automatic updates
* Grouping/LFG system
* Administration
  * Basic moderation tools
  * Group management
* Games/fun commands
  * Play with other users or against AI
* Music
* Destiny 2 integration (possibly)

The roadmap will change over time as development plans become less vague.

## Contributing
See the [contribution guide](https://github.com/trvlrr/ghost/wiki/Contribution-guide) for information on contributing to the project.

[gpl]: https://www.gnu.org/graphics/gplv3-88x31.png
[jda]: https://i.imgur.com/SehEwtJ.png
[ ![gpl][] ](https://www.gnu.org/licenses/licenses.html#GPL)
[ ![jda][] ](https://github.com/DV8FromTheWorld/JDA)