/*
 * This file is part of ghost.
 *
 * ghost is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ghost is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ghost.  If not, see <http://www.gnu.org/licenses/>.
 */

package util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Provides an easy way to access the bot's properties file.
 */
public class Settings {
    private static final Logger LOG = LoggerFactory.getLogger(Settings.class);
    private Properties properties = new Properties();

    /**
     * @param configFile Path to the configuration file to use.
     */
    public Settings(String configFile) {
        try {
            properties.load(new BufferedReader(new FileReader(configFile)));
        } catch(FileNotFoundException ex) {
            LOG.error("Could not find specified configuration file.");
            ex.printStackTrace();
        } catch(IOException ex) {
            LOG.error("Unspecified IO error loading configuration file.");
            ex.printStackTrace();
        }
    }

    /**
     * Fetches a setting from the configuration file.
     * @param key Name of setting to get. Should be one of preset Key enums.
     * @return Value of setting.
     */
    public String get(Keys key) {
        return properties.getProperty(key.toString());
    }

    /**
     *
     * @param key Name of setting to set. Should be one of preset Key enums.
     * @param value Value to set the setting to.
     * @return Previous value of setting.
     */
    public Object set(Keys key, String value) {
        return properties.setProperty(key.toString(), value);
    }

    /**
     * Defines Keys to be used in Settings.get() and Settings.set().
     */
    public enum Keys {
        TOKEN("TOKEN"),
        PREFIX("PREFIX"),
        UPDATE("UPDATE"),
        NICKNAME("NICK"),
        LOG("LOGGING"),
        MODULE_MUSIC("M_MUSIC"),
        MODULE_LFG("M_LFG"),
        MODULE_FUN("M_FUN"),
        MODULE_ADMIN("M_ADMIN");

        private final String key;

        private Keys(final String key) {
            this.key = key;
        }

        @Override
        public String toString() {
            return key;
        }
    }
}