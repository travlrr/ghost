/*
 * This file is part of ghost.
 *
 * ghost is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ghost is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ghost.  If not, see <http://www.gnu.org/licenses/>.
 */

package bot;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Settings;
import util.Settings.Keys;

/**
 * The "mother" class for the entire bot. Initializes and connects all shards as well as other essential stuff.
 */
public class Container {
    private static final Logger LOG = LoggerFactory.getLogger(Container.class);
    private JDA api;
    private Settings settings;

    /**
     * Initializes and connects bot automatically
     * @param configFile Path to configuration file for bot
     */
    public Container(String configFile) {
        settings = new Settings(configFile);
        JDABuilder builder = new JDABuilder(AccountType.BOT).setToken(settings.get(Keys.TOKEN));
        // TODO: Implement util.Database and initialize it here
        // blah blah blah I just need to commit I'll lay out the rest of this class later
    }
}