/*
 * This file is part of ghost.
 *
 * ghost is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ghost is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ghost.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        boolean uiMode;
        if(args[0].equals("true")) {
            startUi();
        } else if(args[0].equals("false")) {
            startCmd();
        } else {
            System.out.println("Please specify either 'true' or 'false' to enable or disable the GUI when starting Ghost.");
            System.exit(0);
        }
    }

    public static void startUi() {
        /* TODO: Start graphical interface
         * Need to finish UI first before being able to start in UI mode.
         * Getting a working command line interface is currently prioritized over this.
         */
        System.out.println("Ghost's graphical UI is currently not available. Starting in command line mode.");
        startCmd();
    }

    public static void startCmd() {
        /* TODO: Start command line interface
         * Internal bot code needs to be worked out before we can start the program.
         * This, obviously, is prioritized over starting the bot. Nobody wants half of a bot.
         */
        System.exit(0);
    }
}